#include "TP2.h"
#include "tests.h"




/**
 * @brief Point d'entrée
 * 
 * @param argc Nombre d'arguments
 * @param argv Arguments
 * @return int Code d'erreur
 */
int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	testTemp();

	testDistribution(1000);
	testDistribution(1000000);

	double observationHDL[6] = {100, 400, 600, 400, 100, 200};
	empiricalDistribution(6, observationHDL, 1000);
	empiricalDistribution(6, observationHDL, 1000000);

	testNegativeExponential(10, 1000);
	testNegativeExponential(10, 1000000);

	test20Bins(1000);
	test20Bins(1000000);

	testCommonDice(1000);
	testCommonDice(1000000);

	
	testBoxMuller(1000);
	testBoxMuller(1000000);

	return 0;
}
