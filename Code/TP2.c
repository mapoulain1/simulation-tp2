#include "TP2.h"
#include "color.h"
#include "tests.h"


#include "mt19937ar.h"
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b]
 * 
 * @param a a
 * @param b b
 * @return double nombre pseudo aléatoire
 */
double uniform(double a, double b) {
	return genrand_real1() * (b - a) + a;
}


/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b[]
 * 
 * @param a a
 * @param b b
 * @return double Nombre pseudo aléatoire
 */
double uniform_exclusive(double a, double b) {
	return genrand_real2() * (b - a) + a;
}


/**
 * @brief Réalise une distribution empirique à partir d'une distribution observée
 * 
 * @param numberOfClasses Nombre de classes dans la distribution
 * @param distribution La distribution observé
 * @param numberOfIteration Le nombre d'itération à calculer
 */
void empiricalDistribution(int numberOfClasses, const double observation[], int numberOfIteration) {
	int *distributionOut = malloc(numberOfClasses * sizeof(int));
	double *cumulative = calloc(sizeof(double), numberOfClasses);
	double rand;
	double sum = 0;

	if (!distributionOut || !cumulative) {
		fprintf(stderr, "Malloc error\n");
		return;
	}

	for (int i = 0; i < numberOfClasses; i++) {
		distributionOut[i] = 0;
		sum += observation[i];
	}

	cumulative[0] = observation[0];

	for (int i = 1; i < numberOfClasses; i++) {
		cumulative[i] = cumulative[i - 1] + observation[i];
	}

	for (int i = 0; i < numberOfIteration; i++) {
		rand = genrand_real1();

		for (int j = 0; j < numberOfClasses; j++) {
			if (rand < cumulative[j] / sum) {
				distributionOut[j]++;
				break;
			}
		}
	}

	printf(RED "For %d drawings : \n" RESET, numberOfIteration);

	for (int i = 0; i < numberOfClasses; i++) {
		printf("Species %d : %.4lf %%\n", i, 100 * (distributionOut[i] / (double)numberOfIteration));
	}

	free(distributionOut);
	free(cumulative);
}


/**
 * @brief Retourne un nombre pseudo aléatoire suivante une loi exponentielle négative
 * 
 * @param mean Moyenne de la loi
 * @return double Nombre pseudo aléatoire
 */
double negativeExponential(double mean) {
	return -mean * log(1 - genrand_real1());
}


/**
 * @brief Lance 20 dés 6 et retourne la somme des faces
 * 
 * @return int Somme des faces
 */
int random20Dice(void) {
	int sum = 0;

	for (int i = 0; i < 20; i++) {
		sum += (int)uniform_exclusive(1, 7);
	}

	return sum;
}


/**
 * @brief Lance numberOfIteration fois 20 dés
 * 
 * @param numberOfIteration Nombre d'itération à calculer
 * @param outMean Moyenne observée
 * @param outDeviation Écart-type observé
 * @param outBinsArray Résultat
 */
void randomDice(int numberOfIteration, double *outMean, double *outDeviation, double *outBinsArray) {
	double sum = 0;
	double *values = calloc(sizeof(double), numberOfIteration);
	double binRange = (double)(120 - 20) / ((double)NUMBER_OF_BINS + 1.0);
	double cumulativeBin[NUMBER_OF_BINS] = {0};

	if (!values) {
		fprintf(stderr, "Malloc error\n");
		return;
	}

	for (int i = 0; i < numberOfIteration; i++) {
		values[i] = random20Dice();

		sum += (int)values[i];
	}

	*outMean = sum / numberOfIteration;
	*outDeviation = deviation(numberOfIteration, values);

	cumulativeBin[0] = 20;
	for (int i = 1; i < NUMBER_OF_BINS; i++) {
		cumulativeBin[i] += cumulativeBin[i - 1] + binRange;
	}

	for (int i = 0; i < numberOfIteration; i++) {
		for (int j = 0; j < NUMBER_OF_BINS; j++) {
			if (values[i] < cumulativeBin[j]) {
				outBinsArray[j]++;
				break;
			}
		}
	}

	free(values);
}


/**
 * @brief Calcule l'écart-type d'un echantillon
 * 
 * @param numberOfValues Nombre de valeurs dans l'échantillon
 * @param values Échantillon
 * @return double Écart-type
 */
double deviation(int numberOfValues, double values[]) {
	double sum = 0;
	double mean;
	double sumDeviation = 0;

	for (int i = 0; i < numberOfValues; i++) {
		sum += values[i];
	}

	mean = sum / numberOfValues;

	for (int i = 0; i < numberOfValues; i++) {
		sumDeviation += fabs(values[i] - mean) * fabs(values[i] - mean);
	}
	
	return sqrt(sumDeviation / numberOfValues);
}


/**
 * @brief Calcule 2 nombres pseudo aléatoires avec la méthode de Box et Muller
 * 
 * @param mean Moyenne de la loi normale
 * @param deviation Écart-type de la loi normale
 * @param outNumber1 Nombre 1
 * @param outNumber2 Nombre 2
 */
void boxMuller(double mean, double deviation, double *outNumber1, double *outNumber2) {
	double rn1;
	double rn2;
	double rootDeviation = sqrt(deviation);

	rn1 = genrand_real2();
	rn2 = genrand_real2();

	*outNumber1 = cos(2 * M_PI * rn2) * sqrt(-2 * log2(rn1)) * rootDeviation + mean;
	*outNumber2 = sin(2 * M_PI * rn1) * sqrt(-2 * log2(rn2)) * rootDeviation + mean;
}


