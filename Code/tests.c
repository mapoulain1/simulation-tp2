#include "tests.h"


#include "color.h"
#include "TP2.h"

#include "mt19937ar.h"
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>



void testTemp(void) {
	double sum = 0;
	double min = DBL_MAX;
	double max = DBL_MIN;
	double random;

    printf("\n--- TEST TEMPERATURE --- \n\n");

	for (int i = 0; i < 1000; i++) {
		random = uniform(-89.2, 56.7);
		sum += random;

		if (random > max)
			max = random;
		if (random < min)
			min = random;
	}
    
	printf("Average : %.4lf\n", sum / 1000.0);
	printf("Range : [%.4lf : %.4lf]\n", min, max);
}

void testDistribution(int numberOfIteration) {
	int distribution[3] = {0, 0, 0};
	double rand;

    printf("\n--- TEST DISTRIBUTION 3 CLASSES --- \n\n");

	for (int i = 0; i < numberOfIteration; i++) {
		rand = genrand_real1();

		if (rand < 0.5)
			distribution[0]++;
		else if (rand < 0.6)
			distribution[1]++;
		else
			distribution[2]++;
	}

	printf(RED "For %d drawings : \n" RESET, numberOfIteration);
	printf("Species A : %.4lf %%\n", 100 * (distribution[0] / (double)numberOfIteration));
	printf("Species B : %.4lf %%\n", 100 * (distribution[1] / (double)numberOfIteration));
	printf("Species C : %.4lf%%\n\n", 100 * (distribution[2] / (double)numberOfIteration));
}


void testNegativeExponential(double mean, int numberOfIteration) {
	double sum = 0;

    printf("\n--- TEST EXPONENTIELLE --- \n\n");

	for (int i = 0; i < numberOfIteration; i++) {
		sum += negativeExponential(mean);
	}

	printf(RED "For %d drawings with %.4lf mean: \n" RESET, numberOfIteration, mean);
	printf("Mean : %.4lf\n", sum / numberOfIteration);
}

void test20Bins(int numberOfIteration) {
	long bins[21] = {0};

    printf("\n--- TEST EXPONENTIELLE CLASSES --- \n\n");

	for (int i = 0; i < numberOfIteration; i++) {
		int intRandom = negativeExponential(10);
		bins[intRandom > 20 ? 20 : intRandom]++;
	}

	printf(RED "For %d drawings : \n" RESET, numberOfIteration);

	for (int i = 0; i < 21; i++) {
		printf("Bin n°%d : %ld \n", i + 1, (bins[i]));
	}
}



void testCommonDice(int numberOfIteration) {
	double mean;
	double deviation;
	double bins[NUMBER_OF_BINS] = {};

    printf("\n--- TEST DÉ --- \n\n");

	randomDice(numberOfIteration, &mean, &deviation, bins);

	printf("%lf %lf\n", mean, deviation);

	for (int i = 0; i < NUMBER_OF_BINS; i++) {
		printf("%d : %.4lf\n",i, (bins[i]));
	}
}


void testBoxMuller(int numberOfIteration) {
	const int mean = 0;
	const int deviation = 1;

	long bins[NUMBER_OF_BINS_BOX_MULLER] = {0};
	double rand1;
	double rand2;
	double sum = 0;
	int binIndex;

    printf("\n--- TEST BOX ET MULLER --- \n\n");

	for (int i = 0; i < numberOfIteration; i++) {
		boxMuller(mean, deviation, &rand1, &rand2);

		if (rand1 < -4) {
			binIndex = 0;
		} else if (rand1 > 4) {
			binIndex = NUMBER_OF_BINS_BOX_MULLER-1;
		} else {
			binIndex = rand1 * (NUMBER_OF_BINS_BOX_MULLER/10) + (NUMBER_OF_BINS_BOX_MULLER/2);
		}
		bins[binIndex]++;

		sum += rand1;
		sum += rand2;
	}
    
	for (int i = 1; i < NUMBER_OF_BINS_BOX_MULLER-1; i++) {
		printf("%d : %ld\n",i,bins[i]);
	}

	printf("Mean : %.6lf\n", sum / (numberOfIteration * 2));
}