#ifndef TESTS_H_
#define TESTS_H_



void testTemp(void);

void testDistribution(int numberOfIteration);

void testNegativeExponential(double mean, int numberOfIteration);

void test20Bins(int numberOfIteration);

void testCommonDice(int numberOfIteration);

void testBoxMuller(int numberOfIteration);


#endif