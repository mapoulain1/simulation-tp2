#ifndef TP2_H
#define TP2_H

#define NUMBER_OF_BINS 100
#define NUMBER_OF_BINS_BOX_MULLER 101

/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b]
 * 
 * @param a a
 * @param b b
 * @return double nombre pseudo aléatoire
 */
double uniform(double a, double b);


/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b[]
 * 
 * @param a a
 * @param b b
 * @return double Nombre pseudo aléatoire
 */
double uniform_exclusive(double a, double b);


/**
 * @brief Réalise une distribution empirique à partir d'une distribution observée
 * 
 * @param numberOfClasses Nombre de classes dans la distribution
 * @param distribution La distribution observé
 * @param numberOfIteration Le nombre d'itération à calculer
 */
void empiricalDistribution(int numberOfClasses, const double distribution[], int numberOfIteration);

/**
 * @brief Retourne un nombre pseudo aléatoire suivante une loi exponentielle négative
 * 
 * @param mean Moyenne de la loi
 * @return double Nombre pseudo aléatoire
 */
double negativeExponential(double mean);

/**
 * @brief Lance 20 dés 6 et retourne la somme des faces
 * 
 * @return int Somme des faces
 */
int random20Dice(void);

/**
 * @brief Lance numberOfIteration fois 20 dés
 * 
 * @param numberOfIteration Nombre d'itération à calculer
 * @param outMean Moyenne observée
 * @param outDeviation Écart-type observé
 * @param outBinsArray Résultat
 */
void randomDice(int numberOfIteration, double *outMean, double *outDeviation, double *outBinsArray);

/**
 * @brief Calcule l'écart-type d'un echantillon
 * 
 * @param numberOfValues Nombre de valeurs dans l'échantillon
 * @param values Échantillon
 * @return double Écart-type
 */
double deviation(int numberOfValues, double values[]);

/**
 * @brief Calcule 2 nombres pseudo aléatoires avec la méthode de Box et Muller
 * 
 * @param mean Moyenne de la loi normale
 * @param deviation Écart-type de la loi normale
 * @param outNumber1 Nombre 1
 * @param outNumber2 Nombre 2
 */
void boxMuller(double mean, double deviation, double *outNumber1, double *outNumber2);


#endif